import React from 'react';
import $ from 'jquery';

var Header = React.createClass({
  render: function() {
    return (
      <h1>Employee Directory</h1>
    );
  }
});

var SearchBar = React.createClass({
  render: function() {
    return (
      <div className="search-bar">
        <input type="text" />
      </div>
    );
  }
});

var EmployeeList = React.createClass({
  render: function() {
    return (
      <div className="items">
        {this.props.lists.map(item => 
          <EmployeeListItem key={item.name} item={item} />
        )}
      </div>
    );
  }
});

var EmployeeListItem = React.createClass({
  render: function() {
    var item = this.props.item;
    return (
      <div className="item">
        <img src={item.imgUrl} alt={item.name} />
        <h6>{item.name}</h6>
        <div className="position">{item.position}</div>
      </div>
    );
  }
});

var Homepage = React.createClass({
  getInitialState: function() {
    return {
      lists: []
    };
  },
  componentDidMount: function() {
    var _t = this;
    $.ajax({
      url: '/api/getlists.json',
      type: 'GET',
      dataType: 'json'
    }).done(function(data) {
      _t.setState({
        lists: data
      });
    });
  },
  render: function() {
    return (
      <div className="page-home">
        <Header />
        <SearchBar />
        <EmployeeList lists={this.state.lists} />
      </div>
    );
  }
});

var App = React.createClass({
  render: function() {
    return (
      <Homepage />
    );
  }
});

export default App;
